'use strict'

const Store = require('electron-store')

class DataStorePegawai extends Store {
  constructor (settings) {
    super(settings)

    // initialize with pegawai or empty array
    this.pegawai = this.get('pegawai') || []
  }

  savePegawai () {
    // save pegawai to JSON file
    this.set('pegawai', this.pegawai)

    // returning 'this' allows method chaining
    return this
  }

  getPegawai () {
    // set object's pegawai to pegawai in JSON file
    this.pegawai = this.get('pegawai') || []

    return this
  }

  addPegawai (_pegawai) {
    // merge the existing pegawai with the new pegawai
    this.pegawai = [ ...this.pegawai, _pegawai ]

    return this.savePegawai()
  }

  deletePegawai (_pegawai) {
    // filter out the target pegawai
    this.pegawai = this.pegawai.filter(t => t !== _pegawai)

    return this.savePegawai()
  }
}

module.exports = DataStorePegawai
