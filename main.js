const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
var com = new SerialPort('/dev/ttyUSB0', { autoOpen: false }, {
  baudRate: 9600
})


const parser = new Readline()
'use strict'

const path = require('path')
const { app, ipcMain } = require('electron')

const Window = require('./Window')
const DataStoreOrder = require('./DataStoreOrder')
const DataStorePegawai = require('./DataStorePegawai')
const DataStorePort = require('./DataStorePort')

require('electron-reload')(__dirname)

// create a new store name
const orderData = new DataStoreOrder({ name: 'Order Main' })
const pegawaiData = new DataStorePegawai({ name: 'Pegawai Main' })
const portData = new DataStorePort({ name: 'Port Main' })

function main() {
  // list window
  let mainWindow = new Window({
    file: path.join('renderer', 'index.html')
  })

  // add window
  let addOrderWin
  let addPegawaiWin
  let addPortWin

  // put these events into their own file

  // initialize
  mainWindow.once('show', () => {
    mainWindow.webContents.send('order', orderData.order)
    mainWindow.webContents.send('pegawai', pegawaiData.pegawai)
    mainWindow.webContents.send('port', portData.port)

  })
  
  const updatedPort = portData.getPort().port[0]
  console.log(typeof updatedPort)
  if(typeof updatedPort != "undefined"){
    
      SerialPort.list((err, ports) => {
        ports.forEach(port => {
          if(typeof port.manufacturer==="string"){
            if(port.comName===portData.getPort().port[0])
              console.log(port.comName)
              com = new SerialPort(port.comName, { baudRate: 9600 })
              com.open(function (err) {
                if (err) {
                  return console.log('Error opening port: ', err.message)
                }
                
              })
              com.pipe(parser)
          }
        })
      })
  }
  // create add order window
  ipcMain.on('add-order-window', () => {
    // if addOrderWin does not already exist
    if (!addOrderWin) {
      // create a new add order window
      addOrderWin = new Window({
        file: path.join('renderer', 'addOrder.html'),
        width: 400,
        height: 400,
        // close with the main window
        parent: mainWindow
      })

      // cleanup
      addOrderWin.on('closed', () => {
        addOrderWin = null
      })
    }
  })

  ipcMain.on('add-pegawai-window', () => {
    // if addPegawaiWin does not already exist
    if (!addPegawaiWin) {
      // create a new add pegawai window
      addPegawaiWin = new Window({
        file: path.join('renderer', 'addPegawai.html'),
        width: 400,
        height: 400,
        // close with the main window
        parent: mainWindow
      })

      // cleanup
      addPegawaiWin.on('closed', () => {
        addPegawaiWin = null
      })
    }
  })

  ipcMain.on('add-port-window', () => {
    // if addPortWin does not already exist
    if (!addPortWin) {
      // create a new add port window
      addPortWin = new Window({
        file: path.join('renderer', 'serialport.html'),
        width: 400,
        height: 400,
        // close with the main window
        parent: mainWindow
      })

      // cleanup
      addPortWin.on('closed', () => {
        addPortWin = null
      })
    }
  })

  //read serial
  ipcMain.on('read-serial', () => {
    com.write("NEXT\n"+'READ\n', function(err) {
        
      if (err) {
        return console.log('Error on write: ', err.message)
      }
      console.log('message written')
    })
    
  })


  // add-order from add order window
  ipcMain.on('add-order', (event, order) => {
    const updatedOrder = orderData.addOrder(order).order

    mainWindow.send('order', updatedOrder)
  })

  ipcMain.on('add-pegawai', (event, pegawai) => {
    const updatedPegawai = pegawaiData.addPegawai(pegawai).pegawai

    mainWindow.send('pegawai', updatedPegawai)
  })

  ipcMain.on('add-port', (event, port) => {

    console.log(typeof portData.getPort().port[0])
    if(typeof portData.getPort().port[0]==='undefined'){ 
    } else{
      com.close()
      portData.deletePort(portData.getPort().port[0])
    }
    const updatedPort = portData.addPort(port).port
    SerialPort.list((err, ports) => {
      ports.forEach(port => {
        if(typeof port.manufacturer==="string"){
  
          if(port.comName===portData.getPort().port[0])
          console.log("Nama port :")
            console.log(port.comName)
            com = new SerialPort(port.comName, { baudRate: 9600 })
            com.open(function (err) {
              if (err) {
                return console.log('Error opening port in add-port: ', err.message)
              }
            })
            com.pipe(parser)
          }
      })
    })
    mainWindow.send('port', updatedPort)
    //addPortWin.send('port', updatedPort)
  })

  parser.on('data', function (data) {
    console.log('Data:', data)
    mainWindow.send('display-data', data)
  })

  ipcMain.on('delete-order', (event, order) => {
    const updatedOrder = orderData.deleteOrder(order).order

    mainWindow.send('order', updatedOrder)
  })

  ipcMain.on('delete-pegawai', (event, pegawai) => {
    const updatedPegawai = pegawaiData.deletePegawai(pegawai).pegawai

    mainWindow.send('pegawai', updatedPegawai)
  })

  ipcMain.on('delete-port', (event, port) => {
    const updatedPort = portData.deletePort(port).port
    com.close()
    mainWindow.send('port', updatedPort)
  })

  ipcMain.on('send-order', (event, order) => {
    const updatedOrder = orderData.getOrder()

    com.write("NEXT\n"+'WRITE '+order+'\n', function(err) {
        
      if (err) {
        return console.log('Error on write: ', err.message)
      }
      console.log('message written')
    })

      console.log("NEXT\n"+'WRITE '+order+'#'+'\n');
      console.log(JSON.stringify(orderData.getOrder().order))
  })

  ipcMain.on('send-pegawai', (event, pegawai) => {
    const updatedPegawai = pegawaiData.getPegawai()

      com.write("NEXT\n"+'WRITE '+pegawai+'\n', function(err) {
        
        if (err) {
          return console.log('Error on write: ', err.message)
        }
        console.log('message written')
      })
      console.log(JSON.stringify(pegawaiData.getPegawai().pegawai))
      console.log("NEXT\n"+'WRITE '+pegawai+'\n')

  })
}

app.on('ready', main)

app.on('window-all-closed', function () {
  app.quit()
})
