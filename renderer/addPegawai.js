'use strict'

const { ipcRenderer } = require('electron')

const selectpegawai = (e) => {
	ipcRenderer.send('add-pegawai', e.target.textContent)
	e.target.style.backgroundColor = "#000f80";
	e.target.style.color = "#ffffff";
  
  }

async function getPegawai(){
	let mesinId = 1;
	let token = localStorage.getItem('token');
	let data = await fetch('https://be.dcs.stechoq.com/utils/listEmployees',{
		method: 'GET',
		headers: {
			'Content-type': 'application/json',
			'Authorization' : 'Bearer '+token
		}
	})
	.then(response => response.json())
	.then(response => {
		//console.log(response);
		return response;
	})
	.catch(error => {
		console.error(error);
		return {status:false};
	});

	return data;
}

document.getElementById('pegawaiForm').addEventListener('submit', (evt) => {
  // prevent default refresh functionality of forms
  evt.preventDefault()

  // input on the form
  const input = evt.target[0]

  // send pegawai to main process
  ipcRenderer.send('add-pegawai', input.value)

  // reset input
  input.value = ''

  

  
})
var spacer = "                    "
var html = '';
 
  getPegawai().then((result)=>{
	for (var i in result.data){  
		html += `<ul class="list-pegawai"><li class="pegawai-item">${result.data[i].name+"<br>"+result.data[i].id}</li></ul>`
		console.log(result.data[i])
	}
	html+=''
    pegawaiList.innerHTML = html;
    console.log(html)
    pegawaiList.querySelectorAll('.pegawai-item').forEach(item => {
      item.addEventListener('click', selectpegawai)
	
    })
  })

