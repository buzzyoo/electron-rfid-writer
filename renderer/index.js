
'use strict'

const { ipcRenderer } = require('electron')


const deleteorder = (e) => {
  ipcRenderer.send('delete-order', e.target.textContent)
}

const deleteport = (e) => {
  ipcRenderer.send('delete-port', e.target.textContent)
}

const deletepegawai = (e) => {
  ipcRenderer.send('delete-pegawai', e.target.textContent)
}

const sendpegawai = (e) => { console.log
  var i;
  var pos;
  var id_pegawai='';
  console.log(e.target.textContent.length)
  for (i = 0; i < e.target.textContent.length; i++) {
    //text += cars[i] + "<br>";
    if (e.target.textContent[i] >= '0' && e.target.textContent[i] <= '9') {
      // it is a number
      pos = i
      break
    } 
  } 
  for (i = pos; i <e.target.textContent.length; i++) {
    //text += cars[i] + "<br>";
    id_pegawai+=e.target.textContent[i]
  } 
  console.log(id_pegawai)
  ipcRenderer.send('send-pegawai', id_pegawai)
  e.target.style.backgroundColor = "#000f80";
	e.target.style.color = "#ffffff";
  //console.log('hihi')
}

const sendorder = (e) => {
    
  ipcRenderer.send('send-order', e.target.textContent)
  e.target.style.backgroundColor = "#000f80";
	e.target.style.color = "#ffffff";
  //console.log('hihi')
}

const sendport = (e) => { 
  ipcRenderer.send('send-port', e.target.textContent)
  //console.log('hihi')
}



// create add order window button
document.getElementById('createOrderBtn').addEventListener('click', () => {
  ipcRenderer.send('add-order-window')
})

document.getElementById('createPegawaiBtn').addEventListener('click', () => {
  ipcRenderer.send('add-pegawai-window')
})

document.getElementById('createPortBtn').addEventListener('click', () => {
  ipcRenderer.send('add-port-window')
})

document.getElementById('readBtn').addEventListener('click', () => {
  ipcRenderer.send('read-serial')
})

// on receive order
ipcRenderer.on('order', (event, order) => {
  // get the orderList ul
  const orderList = document.getElementById('orderList')

  // create html string
  const orderItems = order.reduce((html, order) => {
    html += `<li class="order-item">${order}</li>`

    return html
  }, '')

  // set list html to the order items
  orderList.innerHTML = orderItems
  

  // add click handlers to delete the clicked order
  orderList.querySelectorAll('.order-item').forEach(item => {
    item.addEventListener('click', sendorder)
    item.addEventListener('contextmenu', deleteorder)
  })
})

  ipcRenderer.on('pegawai', (event, pegawai) => {
    // get the pegawaiList ul
    var i

   
    
   
   
    
    const pegawaiList = document.getElementById('pegawaiList')
  
    // create html string
    const pegawaiItems = pegawai.reduce((html, pegawai) => {
      var nama_pegawai='';
      var pos;
      var id_pegawai='';
      console.log(pegawai.length)
      for (i = 0; i < pegawai.length; i++) {
        //text += cars[i] + "<br>";
        if (pegawai[i] >= '0' && pegawai[i] <= '9') {
          // it is a number
          pos = i
          break
        } 
        nama_pegawai+=pegawai[i]
      } 
      for (i = pos; i < pegawai.length; i++) {
        //text += cars[i] + "<br>";
        id_pegawai+=pegawai[i]
      } 



      html += `<ul class="list-pegawai"><li class="pegawai-item">${nama_pegawai+"<br>"+id_pegawai}</li></ul>`
  
      return html
    }, '')
  
    // set list html to the pegawai items
    pegawaiList.innerHTML = pegawaiItems
  
    // add click handlers to delete the clicked pegawai
    pegawaiList.querySelectorAll('.pegawai-item').forEach(item => {
      item.addEventListener('click', sendpegawai)
      item.addEventListener('contextmenu', deletepegawai)
    })
})

ipcRenderer.on('display-data', (event,data) => {
  document.getElementById('readData').innerHTML = data
})


// on receive port
ipcRenderer.on('port', (event, port) => {
  // get the portList ul
  const portList = document.getElementById('portList')

  // create html string
  const portItems = port.reduce((html, port) => {
    html += `<li class="port-item">${port}</li>`

    return html
  }, '')

  // set list html to the port items
  portList.innerHTML = portItems
  

  // add click handlers to delete the clicked port
  portList.querySelectorAll('.port-item').forEach(item => {
    item.addEventListener('contextmenu', deleteport)
  })
})


setInterval(function(){ console.log("Hello"); }, 3000);
//tes
