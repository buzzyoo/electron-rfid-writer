'use strict'

const { ipcRenderer } = require('electron')

const selectorder = (e) => {
	ipcRenderer.send('add-order', e.target.textContent)
	e.target.style.backgroundColor = "#000f80";
	e.target.style.color = "#ffffff";
  
  }

async function getOrder(){
	let mesinId = 1;
	let token = localStorage.getItem('token');
	let data = await fetch('https://be.dcs.stechoq.com/utils/listOrderNumber/0',{
		method: 'GET',
		headers: {
			'Content-type': 'application/json',
			'Authorization' : 'Bearer '+token
		}
	})
	.then(response => response.json())
	.then(response => {
		//~ console.log(response);
		return response;
	})
	.catch(error => {
		console.error(error);
		return {status:false};
	});

	return data;
}

document.getElementById('orderForm').addEventListener('submit', (evt) => {
  // prevent default refresh functionality of forms
  evt.preventDefault()

  // input on the form
  const input = evt.target[0]

  // send order to main process
  ipcRenderer.send('add-order', input.value)

  // reset input
  input.value = ''

  

  
})
var html = '';
 
  getOrder().then((result)=>{
	for (var i in result.data){  
		html += `<li class="order-item">${result.data[i]}</li>`
		console.log(result.data[i])
	}
	html+=''
    orderList.innerHTML = html;
    console.log(html)
    orderList.querySelectorAll('.order-item').forEach(item => {
      item.addEventListener('click', selectorder)
	
    })
  })

