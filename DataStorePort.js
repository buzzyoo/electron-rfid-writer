'use strict'

const Store = require('electron-store')

class DataStorePort extends Store {
  constructor (settings) {
    super(settings)

    // initialize with port or empty array
    this.port = this.get('port') || []
  }

  savePort () {
    // save port to JSON file
    this.set('port', this.port)

    // returning 'this' allows method chaining
    return this
  }

  getPort () {
    // set object's port to port in JSON file
    this.port = this.get('port') || []

    return this
  }

  addPort (_port) {
    // merge the existing port with the new port
    this.port = [ ...this.port, _port ]

    return this.savePort()
  }

  deletePort (_port) {
    // filter out the target port
    this.port = this.port.filter(t => t !== _port)

    return this.savePort()
  }

  
}

module.exports = DataStorePort
