'use strict'

const Store = require('electron-store')

class DataStoreOrder extends Store {
  constructor (settings) {
    super(settings)

    // initialize with order or empty array
    this.order = this.get('order') || []
  }

  saveOrder () {
    // save order to JSON file
    this.set('order', this.order)

    // returning 'this' allows method chaining
    return this
  }

  getOrder () {
    // set object's order to order in JSON file
    this.order = this.get('order') || []

    return this
  }

  addOrder (_order) {
    // merge the existing order with the new order
    this.order = [ ...this.order, _order ]

    return this.saveOrder()
  }

  deleteOrder (_order) {
    // filter out the target order
    this.order = this.order.filter(t => t !== _order)

    return this.saveOrder()
  }
}

module.exports = DataStoreOrder
